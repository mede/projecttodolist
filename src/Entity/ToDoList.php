<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="item")
     */
    private $items;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="relationTodo", cascade={"persist", "remove"})
     */
    private $relationUser;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setItem($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getItem() === $this) {
                $item->setItem(null);
            }
        }

        return $this;
    }

    public function getRelationUser(): ?User
    {
        return $this->relationUser;
    }

    public function setRelationUser(?User $relationUser): self
    {
        $this->relationUser = $relationUser;

        // set (or unset) the owning side of the relation if necessary
        $newRelationTodo = null === $relationUser ? null : $this;
        if ($relationUser->getRelationTodo() !== $newRelationTodo) {
            $relationUser->setRelationTodo($newRelationTodo);
        }

        return $this;
    }


}

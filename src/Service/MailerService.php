<?php


namespace App\Service;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
Use Symfony\Entity\User;

class MailerService extends AbstractController
{
    /**
     * @param $to
     * @param $subject
     * @param $body_text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmail($mailer, $to, $subject, $body_text){

        $email = (new Email())
            ->from('todolist.contact@gmail.com')
            ->to($to)
            ->subject($subject)
            ->text($body_text);

        $mailer->send($email);
            if ($age > 18){
                return true;
            }
            return false;

            // methode send qui return true si l'user a plus de 18 ans sinon false
    }
}
<?php


namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;


class NotificationService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }



    public function send($text)
    {

        $transport = new GmailSmtpTransport('todolist.contact@gmail.com', 'todolisttest');
        $mailer = new Mailer($transport);

        $email = (new Email())
            ->from('todolist.contact@gmail.com')
            ->to('todolist.contact@gmail.com')
            ->subject('Time for Symfony Mailer!')
            ->context([
                'text' => $text,
            ]);

        $mailer->send($email);
    }
}
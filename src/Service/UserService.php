<?php
namespace App\Service;

use App\Entity\User;

Class UserService
{
    public function __construct($email, $firstname, $lastname, $password, $age)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->password = $password;
        $this->age = $age;
        }

    private function email()
    {
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "Email non valide !";
        }
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }


    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastName()
    {
        return $this->lastname;
    }


    private function password()
    {
        $password = 'user-input-pass';

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8 && 40) {
            echo 'Le mot de passe doit comporter entre 8 et 40 caracteres 
                           et doit comporter au moins une majuscule, 
                           un chiffre et un caractère spécial.';
        } else {
            echo 'Mot de passe résistant ! ';
        }
    }

    private function isNotEmpty($var, $type)
    {
        if (!strlen($var) > 0) {
            $this->errors[] = $type . "is empty";
        }
    }

   /* public function isValidAge()
    {
        $date = explode('/', $this->age);
        if (count($date) !== 3 {
            $this->errors [] = "Date non valide (DD,MM,YYYY)";
        }
        $age = (date('Y') - $date[2]);
        if (($date[1] - date)'m')) > 0{
        $age = ($age - 1);
    }

            if (($date[1] - date('m')) == 0 && ($date[0] - date('d')) > 0) {
                $age = ($age - 1);
            }
            if ($age < 13) {
                $this->errors[] = "Vous devez avoir 13 ans ou plus";
            }
        } */

    public function isValid()
    {
        $this->email();
        $this->isNotEmpty($this->firstname, 'firstname');
        $this->isNotEmpty($this->lastname, 'lastname');
        $this->isValidAge();

        if (($this->age) >= 13) {
            return true;
        }
        return false;
    }
}
